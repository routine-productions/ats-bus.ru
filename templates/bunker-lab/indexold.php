<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>ATS-BUS | Заказ билетов на автобусы из Донецка в Москву, Тулу, Воронеж и обратно</title>
    <meta name="description"
          content="Компания «ATS-BUS» осуществляет регулярные пассажирские перевозки из Донецка в Москву и по другим
           ключевым направлениям. В частности, с территории Донбасса мы выполняем комфортные и безопасные рейсы без
           пропусков, виз и досмотров.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:image" content="/templates/bunker-lab/img/pic-bus.png"/>
    <link rel="stylesheet" href="/templates/bunker-lab/css/index.min.css">

    <meta name="google-site-verification" content="mIab7d10SarSb6ebliTwo1SP7wkx_UrMZq8Hf-rPr94"/>
    <meta name='yandex-verification' content='7407dbeb8eabc65f'/>
    <link rel="apple-touch-icon" sizes="57x57" href="/templates/bunker-lab/img/fav/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/templates/bunker-lab/img/fav/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/templates/bunker-lab/img/fav/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/templates/bunker-lab/img/fav/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/templates/bunker-lab/img/fav/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/templates/bunker-lab/img/fav/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/templates/bunker-lab/img/fav/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/templates/bunker-lab/img/fav/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/templates/bunker-lab/img/fav/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/templates/bunker-lab/img/fav/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/templates/bunker-lab/img/fav/favicon-194x194.png" sizes="194x194">
    <link rel="icon" type="image/png" href="/templates/bunker-lab/img/fav/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/templates/bunker-lab/img/fav/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/templates/bunker-lab/img/fav/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/templates/bunker-lab/img/fav/manifest.json">
    <link rel="mask-icon" href="/templates/bunker-lab/img/fav/safari-pinned-tab.svg" color="#ff4d00">
    <link rel="shortcut icon" href="/templates/bunker-lab/img/fav/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="/templates/bunker-lab/img/fav/mstile-144x144.png">
    <meta name="msapplication-config" content="/templates/bunker-lab/img/fav/browserconfig.xml">
    <meta name="theme-color" content="#ff4d00">
</head>
<body>

<header class="Site-Header" id="Site-Header">
    <div class="Top JS-Mobile-Menu">
        <div class="Top-Bar-Wrapper">
            <div class="Top-Bar">
                <a href="/" class="Logo">
                    <object class="Icon" data="/templates/bunker-lab/img/logo-icon.svg" type="image/svg+xml"></object>
                    <object class="Name" data="/templates/bunker-lab/img/logo-base.svg" type="image/svg+xml"></object>
                </a>

                <div class="Hamburger JS-Mobile-Menu-Toggle"><span></span></div>
                <ul class="Phone-Numbers-List">
                    <li class="Phone-Numbers-Item">
                        <small class="Operator">
                            МТС Украина
                        </small>
                        <p class="tel">
                            <jdoc:include type="modules" name="mtc_ua"/>
                        </p>
                    </li>
                    <li class="Phone-Numbers-Item">
                        <small class="Operator">
                            Life:) Украина
                        </small>
                        <p class="tel">
                            <jdoc:include type="modules" name="life_ua"/>
                        </p>
                    </li>
                    <li class="Phone-Numbers-Item">
                        <small class="Operator">
                            МТС Россия
                        </small>
                        <p class="tel">
                            <jdoc:include type="modules" name="mtc_ru"/>
                        </p>
                    </li>
                    <li class="Phone-Numbers-Item">
                        <small class="Operator">
                            Феникс ДНР
                        </small>
                        <p class="tel">
                            <jdoc:include type="modules" name="fenix_dnr"/>
                        </p>
                    </li>
                </ul>
            </div>
        </div>

        <nav class="JS-Mobile-Menu-List">
            <ul class="Navigation JS-Page-Navigation" data-scroll-shift="125">
                <li class="Navigation-Item">
                    <a href="#Routes">Маршруты</a>
                </li>
                <li class="Navigation-Item">
                    <a href="#Advantages">Наши преимущества</a>
                </li>
                <li class="Navigation-Item">
                    <a href="#Discounts">Скидки на билеты</a>
                </li>
                <li class="Navigation-Item">
                    <a href="#About-Us">О компании</a>
                </li>
                <li class="Navigation-Item">
                    <a href="#Contacts">Контакты</a>
                </li>
            </ul>
        </nav>
    </div>

    <div class="Middle JS-Form"
         data-form-email='rysalka9@rambler.ru,olegblud@gmail.com,ats-bus@yandex.ru'
         data-form-subject='Ats-Bus: Заказ билетов'
         data-form-url='templates/bunker-lab/js/data.form.php'
         data-form-method='POST'>
        <h2>Заказ билетов по телефону на рейсы из&nbsp;Донецка в&nbsp;Москву, Тулу, Воронеж и&nbsp;обратно</h2>
        <input type="tel" name='phone' data-input-title='Телефон' placeholder="Введите телефон">
        <button class="Button JS-Form-Button">Перезвоните мне</button>
        <div class="JS-Form-Result"></div>
    </div>
</header>


<div class="JS-Tabs">
    <div class="Routes" id="Routes">
        <ul class="Routes-Captions-List JS-Tabs-Navigation">
            <li class="Routes-Item">
                <a href="#moscow" class="Active">
                    <img src="/templates/bunker-lab/img/pic-moscow.jpg" alt="" class="Route-Img">
                    Донецк <img src="/templates/bunker-lab/img/ico-arrow.png" alt=""> Москва
                </a>
            </li>
            <li class="Routes-Item">
                <a href="#orel">
                    <img src="/templates/bunker-lab/img/pic-orel.jpg" alt="" class="Route-Img">
                    Донецк <img src="/templates/bunker-lab/img/ico-arrow.png" alt=""> Орел
                </a>
            </li>
            <li class="Routes-Item">
                <a href="#belgorod">
                    <img src="/templates/bunker-lab/img/pic-belgorod.jpg" alt="" class="Route-Img">
                    Донецк <img src="/templates/bunker-lab/img/ico-arrow.png" alt=""> Брянск
                </a>
            </li>
            <li class="Routes-Item">
                <a href="#tula">
                    <img src="/templates/bunker-lab/img/pic-tula.jpg" alt="" class="Route-Img">
                    Донецк <img src="/templates/bunker-lab/img/ico-arrow.png" alt=""> Тула
                </a>
            </li>
            <li class="Routes-Item">
                <a href="#voronezh">
                    <img src="/templates/bunker-lab/img/pic-voronezh.jpg" alt="" class="Route-Img">
                    Донецк <img src="/templates/bunker-lab/img/ico-arrow.png" alt=""> Воронеж
                </a>
            </li>
            <li class="Routes-Item">
                <a href="#kursk">
                    <img src="/templates/bunker-lab/img/pic-kursk.jpg" alt="" class="Route-Img">
                    Донецк <img src="/templates/bunker-lab/img/ico-arrow.png" alt=""> Елец
                </a>
            </li>
            <li class="Routes-Item">
                <a href="#rostov">
                    <img src="/templates/bunker-lab/img/pic-rostov.jpg" alt="" class="Route-Img">
                    Донецк <img src="/templates/bunker-lab/img/ico-arrow.png" alt=""> Ростов
                </a>
            </li>
        </ul>
    </div>

    <jdoc:include type="component"/>

    <a href="#Site-Header" class="Order-Button JS-Scroll-Button">Заказать билеты на рейс</a>
</div>

<section class="Advantages" id="Advantages">
    <header>
        <h3>Наши преимущества</h3>

        <p>Почему наши клиенты выбирают ATS-BUS</p>
    </header>
    <ul class="Advantages-List">
        <li class="Advantages-Item">
            <object data="/templates/bunker-lab/img/icon-1.svg" type="image/svg+xml"></object>
            <h4>Чистый и исправный автобус</h4>

            <p>Поездка пройдет безопасно и с комфортом</p>
        </li>
        <li class="Advantages-Item">
            <object data="/templates/bunker-lab/img/icon-2.svg" type="image/svg+xml"></object>
            <h4>Быстрое прохождение таможни</h4>

            <p>Отлаженная система без простоев на границе</p>
        </li>
        <li class="Advantages-Item">
            <object data="/templates/bunker-lab/img/icon-3.svg" type="image/svg+xml"></object>
            <h4>Опытные водители</h4>

            <p>Мы набираем только надёжных и профессиональных специалистов</p>
        </li>
        <li class="Advantages-Item">
            <object data="/templates/bunker-lab/img/icon-4.svg" type="image/svg+xml"></object>
            <h4>Скидки постоянным клиентам</h4>

            <p>При заказе билетов по телефону</p>
        </li>
        <li class="Advantages-Item">
            <object data="/templates/bunker-lab/img/icon-5.svg" type="image/svg+xml"></object>
            <h4>Отправка посылок</h4>

            <p>Безопасная и дешевая доставка небольших отправлений</p>
        </li>
        <li class="Advantages-Item">
            <object data="/templates/bunker-lab/img/icon-6.svg" type="image/svg+xml"></object>
            <h4>Заботливые стюарды</h4>

            <p>Мы всегда заботимся о вашем комфорте</p>
        </li>
    </ul>
</section>

<!--<section id="Discounts" id="Discounts">-->
<!--    <h3>Наши скидки</h3>-->
<!---->
<!--    <ul class="Discounts-List">-->
<!---->
<!--        <li class="Discounts-Item">-->
<!--            <div class="Discount-Icon">-->
<!--                <span>50%</span>-->
<!--            </div>-->
<!--            <p>Детский билет</p>-->
<!--        </li>-->
<!--        <li class="Discounts-Item">-->
<!--            <div class="Discount-Icon">-->
<!--                <span>10%</span>-->
<!--            </div>-->
<!--            <p>Каждая третья поездка</p>-->
<!--        </li>-->
<!--        <li class="Discounts-Item">-->
<!--            <div class="Discount-Icon">-->
<!--                <span>20%</span>-->
<!--            </div>-->
<!---->
<!--            <p>Каждая пятая поездка</p>-->
<!--        </li>-->
<!--        <li class="Discounts-Item">-->
<!--            <div class="Discount-Icon">-->
<!--                <span class="Small">бесплатно</span>-->
<!--            </div>-->
<!--            <p>Каждая десятая поездка</p>-->
<!--        </li>-->
<!--    </ul>-->
<!--    <a href="#Site-Header" class="Warning-Message JS-Scroll-Button">-->
<!--        Скидки предоставляются только при&nbsp;заказе билетов по&nbsp;телефону-->
<!--    </a>-->
<!--</section>-->

<section class="About-Us" id="About-Us">
    <h3>О компании ATS-Bus</h3>
    <article>
        <jdoc:include type="modules" name="about"/>
    </article>
</section>

<div id="Contacts">
    <div class="Node">
        <ul class="Phone-Numbers">
            <li>
                <span class="Operator">МТС Украина</span>

                <div class="tel">

                    <div class="custom">
                        <p>
                            <jdoc:include type="modules" name="mtc_ua"/>
                        </p>
                    </div>
                </div>
            </li>
            <li>
                <span class="Operator">Life:) Украина</span>

                <div class="tel">

                    <div class="custom">
                        <p>
                            <jdoc:include type="modules" name="life_ua"/>
                        </p>
                    </div>
                </div>
            </li>
            <li>
                <span class="Operator">МТС Россия</span>

                <div class="tel">

                    <div class="custom">
                        <p>
                            <jdoc:include type="modules" name="mtc_ru"/>
                        </p>
                    </div>
                </div>
            </li>
            <li>
                <span class="Operator">Феникс ДНР</span>

                <div class="tel">

                    <div class="custom">
                        <p>
                            <jdoc:include type="modules" name="fenix_dnr"/>
                        </p>
                    </div>
                </div>
            </li>
            <li>
                <span class="Operator">e&ndash;mail</span>

                <div class="email">
                    <p>
                        <jdoc:include type="modules" name="email"/>
                    </p>
                </div>
            </li>
        </ul>
    </div>
</div>

<footer class="Copyright">
    <div class="Node">
        <p class="Copyright-Description"> &copy; 2015 <strong>ATS-BUS</strong> | Пассажирские перевозки из&nbsp;Донецка
            в&nbsp; Москву и&nbsp;Тулу</p>

        <p class="Site-Author">
            <object class="Icon" data="/templates/bunker-lab/img/bunker-logo.svg" type="image/svg+xml"></object>
            Сделано в <a href="http://routine.pro/">Routine Productions</a>
        </p>
    </div>
</footer>


<script src='/templates/bunker-lab/index.min.js' type="text/javascript" async></script>

<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type" : "WebSite",
        "name" : "ATS-BUS",
    "@type": "Organization",
        "url": "http://ats-bus.ru",
        "logo": "http://www.example.com/templates/bunker-lab/img/logo.png",
    "sameAs" :
        [
            "http://vk.com/public97368492"
        ],

    "contactPoint" :
    [
    {
        "@type" : "ContactPoint",
            "telephone" : "+3 8 066 524-87-88",
            "contactType" : "sales",
            "areaServed" : "UA"
    }
    ,
    {
        "@type" : "ContactPoint",
            "telephone" : "+3 8 063 780-75-26",
            "contactType" : "sales",
            "areaServed" : "UA"
    }
    ,
    {
        "@type" : "ContactPoint",
            "telephone" : "+7 916 135-23-11",
            "contactType" : "sales",
            "areaServed" : "RU"
    }
    ]
}

</script>

<script src="/templates/bunker-lab/js/analytics.js" type="text/javascript"></script>
<script>
    ga('create', 'UA-71709804-1', 'auto');
    ga('send', 'pageview');
</script>

<script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter34380010 = new Ya.Metrika({ id:34380010, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script>

</body>
</html>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&subset=latin,cyrillic' rel='stylesheet'
      type='text/css'>