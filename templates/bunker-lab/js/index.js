$(document).ready(function () {
    $('nav').clone().appendTo('body').addClass('Nav-Fixed');

    $(window).scroll(function () {
        if ($(window).scrollTop() > 300) {
            $('.Nav-Fixed').css('top', '0');
        } else {
            $('.Nav-Fixed').css('top', '-65px');
        }
    });
});