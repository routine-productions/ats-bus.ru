/*
 * Copyright (c) 2015
 * Data Scripts - Page Preloader
 * Version 1.0.2
 * Create 2015.12.02
 * Author Bunker Labs

 * Usage:
 *
 * Add class name 'JS-Preloader'         - to set preloader Block
 * Add class name 'JS-Preloader-Percent' - to set Element percent changes
 * Add class name 'JS-Preloader-Row'     - to set Element with graphic preloading (width changes)
 * Add attribute  'data-fade-duration'      - to set vertical shift

 * Code structure:
 * <div class="Preloader JS-Preloader">
 *    <div class="JS-Preloader-Row"></div>
 *    <div class="Preloader-Content">SiteName</div>
 *    <div><span class="JS-Preloader-Percent">0</span>%</div>
 * </div>
 */
(function ($) {
    $(document).ready(function () {
        var $Preloader = $('.JS-Preloader'),
            $Preloader_Percent = $('.JS-Preloader-Percent'),
            $Preloader_Row = $('.JS-Preloader-Row'),
            Duration = $Preloader.attr('data-fade-duration') ? $Preloader.attr('data-fade-duration') : 700;

        var Images = [],
            Count_Loaded_Images = 0;

        // Logic
        Get_Images();
        var Count_Images = Images.length;
        Load_Images();

        // Get all Images
        function Get_Images() {
            $('body *:not(script)').each(function (Element_Key, Element) {
                var Img_Background = $(Element).css('background-image'),
                    Img_Src = $(Element).attr('src');

                if (Img_Background && Img_Background != 'none' && Img_Background.indexOf('gradient') == -1) {
                    Img_Background = Img_Background.match(/url\((.*?)\)/)[1];
                    Img_Background = Img_Background.replace(/\"/g,'');


                    Images.push(Img_Background);
                } else if ($(this).prop("tagName") == 'IMG' && Img_Src.length) {
                    Images.push(Img_Src);
                }
            });
        }

        // Load Images
        function Load_Images() {
            $.each(Images, function (Key, Src) {
                var Img = new Image();
                $(Img).load(function () {
                    Load_Image();
                }).error(function () {
                    Load_Image();
                }).attr('src', Src);
            });
        }

        // On Load Images
        function Load_Image() {
            var Load_Percent = Math.round(++Count_Loaded_Images / Count_Images * 100);

            $Preloader_Percent.text(Load_Percent);
            $Preloader_Row.width(Load_Percent + '%');

            if (Load_Percent == 100) {
                $Preloader.fadeOut(Duration);
            }
        }
    });
})(jQuery);