/*
 * Copyright (c) 2015
 * Data Scripts - Page Navigation
 * Version 1.0.0
 * Created 2015.12.22
 * Author Bunker Labs

 * Usage:
 * Add class name 'JS-Page-Navigation' to block with menu items

 * Code structure:
 * <nav class='JS-Page-Navigation'>
 *     <a href='#Section-1'>Section 1</a>
 *     <a href='#Section-2'>Section 2</a>
 *     <a href='#Section-3'>Section 3</a>
 * </nav>
 * <section id='Section-1'></section>
 * <section id='Section-2'></section>
 * <section id='Section-3'></section>
 */
(function ($) {
    $(window).load(function () {

        var Navigation = '.JS-Page-Navigation',
            Data_Shift_Scroll = $(Navigation).attr('data-scroll-shift') ? $(Navigation).attr('data-scroll-shift') : 0,
            Data_Duration = $(Navigation).attr('data-duration') ? $(Navigation).attr('data-duration') : 800;

        $(document).on('click', '.JS-Page-Navigation a', function () {
            var Scroll = $($(this).attr('href')).offset().top - Data_Shift_Scroll;

            $('html,body').animate({'scrollTop': Scroll + 'px'}, Data_Duration);
            history.pushState(null, null, $(this).attr('href'));

            return false;
        });

        var Sizes = [];

        Change_Sizes();
        $(window).resize(function () {
            Change_Sizes();
        });

        function Change_Sizes() {
            $(Navigation).find('a').each(function (Key, Value) {
                Sizes[Key] = [];
                Sizes[Key]['id'] = $(Value).attr('href');
                Sizes[Key]['value'] = $(Sizes[Key]['id']).offset().top - Data_Shift_Scroll;
                Sizes[Key]['height'] = Sizes[Key]['value'] + $(Sizes[Key]['id']).outerHeight();
            });
        }

        $(window).scroll(function () {
            var Scroll = $(window).scrollTop();
            for (var I = 0; I < Sizes.length; I++) {
                if ((Scroll >= Sizes[I]['value'] && Scroll <= Sizes[I]['height'])) {
                    $(Navigation).find('a').removeClass('Active');
                    $('[href=' + Sizes[I]['id'] + ']').addClass('Active');
                }
            }
        });
    });
})(jQuery);