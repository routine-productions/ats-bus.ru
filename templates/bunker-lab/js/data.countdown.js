/*
 * Copyright (c) 2015
 * Data Scripts - Countdown
 * Version 1.0.2
 * Create 2015.12.13
 * Author Bunker Labs

 * Usage:
 *
 * Add class name 'JS-Countdown' - to activate countdown
 * Add attribute data-countdown      - to set finish date: 01 January 2016 00:00:00

 * Code structure:
 * <div class="JS-Countdown" data-countdown="01 January 2016 00:00:00"></div>
 */
(function ($) {
    $(document).ready(function () {
        var Countdown = $('.JS-Countdown');
        Countdown.html(
            '<div class="JS-Days"><div class="JS-Days-Number JS-Number"><span></span></div><div class="JS-Days-Number JS-Number"><span></span></div></div>' +
            '<div class="JS-Hours"><div class="JS-Hours-Number JS-Number"><span></span></div><div class="JS-Hours-Number JS-Number"><span></span></div></div>' +
            '<div class="JS-Minutes"><div class="JS-Minutes-Number JS-Number"><span></span></div><div class="JS-Minutes-Number JS-Number"><span></span></div></div>' +
            '<div class="JS-Seconds"><div class="JS-Seconds-Number JS-Number"><span></span></div><div class="JS-Seconds-Number JS-Number"><span></span></div></div>'
        );

        Countdown.each(function () {
            var Finish_Date = new Date(Countdown.attr('data-countdown')),
                Counter     = $(this),
                Difference  = Finish_Date - new Date();

            if (Difference > 0) {
                function Countdown_Step() {
                    var Current_Date = new Date(),
                        Difference   = Finish_Date - Current_Date,
                        Time         = {};

                    Time = {
                        seconds: Math.floor((Difference / 1000) % 60).toString(),
                        minutes: Math.floor((Difference / 1000 / 60) % 60).toString(),
                        hours  : Math.floor((Difference / (1000 * 60 * 60)) % 24).toString(),
                        days   : Math.floor(Difference / (1000 * 60 * 60 * 24)).toString()
                    };

                    if (!Time.seconds[1]) {
                        Time.seconds = '0' + Time.seconds[0];
                    }
                    if (!Time.minutes[1]) {
                        Time.minutes = '0' + Time.minutes[0];
                    }
                    if (!Time.hours[1]) {
                        Time.hours = '0' + Time.hours[0];
                    }

                    if (!Time.days[1]) {
                        Time.days = '0' + Time.days[0];
                    }

                    Counter.find('.JS-Seconds>.JS-Number:first span').text(Time.seconds[0]);
                    Counter.find('.JS-Seconds>.JS-Number:last span').text(Time.seconds[1]);

                    Counter.find('.JS-Minutes>.JS-Number:first span').text(Time.minutes[0]);
                    Counter.find('.JS-Minutes>.JS-Number:last span').text(Time.minutes[1]);

                    Counter.find('.JS-Hours>.JS-Number:first span').text(Time.hours[0]);
                    Counter.find('.JS-Hours>.JS-Number:last span').text(Time.hours[1]);

                    Counter.find('.JS-Days>.JS-Number:first span').text(Time.days[0]);
                    Counter.find('.JS-Days>.JS-Number:last span').text(Time.days[1]);
                    setTimeout(function () {
                        Countdown_Step();
                    }, 1000);
                }

                Countdown_Step();
            } else {
                Counter.remove();
            }
        });
    });
})(jQuery);