/*
 * Copyright (c) 2015
 * Data Scripts - Scroll on button
 * Version 1.0.1
 * Create 2015.12.18
 * Author Bunker Labs

 * Usage:
 *1)add structure
 *2)add attribute 'href' to the button and attribute 'id' to block


 * Code structure:
 * <a class="JS-Scroll-Button" href="#Block">Scroll to Block 1</a>
 * <div id="Block">Block</div>
 */
(function ($) {
    $(document).ready(function () {

        $('.JS-Scroll-Button').click(function () {

            var Data_Scroll     = $(this).attr('href'),
                Scroll_Position = $(Data_Scroll).offset().top;

            $('body, html').animate({
                scrollTop: Scroll_Position
            });

            return false;
        });
    });
})(jQuery);
