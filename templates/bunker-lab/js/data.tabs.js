/*
 * Copyright (c) 2015
 * Data Scripts - Tabs
 * Version 1.0.1
 * Create 2015.12.17
 * Author Bunker Labs

 * Usage:
 * add attribute 'data-tab' to the list tab and tab content
 * add class "Tab-Content-Item" to the tab content list

 * Code structure:
 *
 * <div class="JS-Tabs">
 *      <ul class="JS-Tabs-Navigation">
 *          <li><a href="#Tab-1" class="Active">Tab 1</a></li>
 *          <li><a href="#Tab-2">Tab 2</a></li>
 *      </ul>
 *      <ul class="JS-Tabs-Content">
 *          <li data-tab='Tab-1'>Content 1</li>
 *          <li data-tab='Tab-1'>Content 1</li>
 *          <li data-tab='Tab-2'>Content 2</li>
 *          <li data-tab='Tab-2'>Content 2</li>
 *          <li data-tab='Tab-2'>Content 2</li>
 *      </ul>
 * </div>
 */
(function ($) {
    $(document).ready(function () {
        $('.JS-Tabs').each(function () {
            var Hash = $(this).find('.JS-Tabs-Navigation .Active').attr('href').replace('#', '');
            $(this).find('.JS-Tabs-Content [data-tab=' + Hash + ']').show().siblings('[data-tab !=' + Hash + ']').hide();
        });

        $('.JS-Tabs .JS-Tabs-Navigation a').click(function () {
            var Hash = $(this).attr('href').replace('#', '');
            $(this).parents('.JS-Tabs-Navigation').find('a').removeClass('Active');

            $(this).parents('.JS-Tabs').find('.JS-Tabs-Content [data-tab=' + Hash + ']').show().siblings('[data-tab !=' + Hash + ']').hide();
            $(this).addClass('Active');

            history.pushState(null, null, "#" + Hash);
            return false;
        });
    });
})(jQuery);