/*
 * Copyright (c) 2015
 * Data Scripts - Modal
 * Version 1.0.1
 * Created 2015.12.03
 * Author Bunker Labs

 * Usage:
 * Add class name 'JS-Modal'               - to set Modal
 * Add class name 'JS-Modal-Box'           - to set Modal Box
 * Add class name 'JS-Modal-Close'         - to set Close Buttons
 * Add class name 'JS-Modal-Button'        - to set Modal Button
 * Add attr 'data-modal-id' to 'JS-Button' - to associate Modal Button with Modal Window
 * Add same id to 'JS-Modal'               - to identify Modal Window
 *
 * Code structure:
 * <div class='JS-Modal' id='Modal-100'>
 *     <div class='JS-Modal-Box'></div>
 *     <div class='Modal-Close'>×</div>
 * </div>
 *
 * <button class="Modal-Button" data-id="Modal-100">Show Modal-100</div>
 */

(function ($) {
    $(document).ready(function () {
        var Modal         = '.JS-Modal',
            $Modal_Box    = $('.JS-Modal-Box'),
            $Modal_Close  = $('.JS-Modal-Close'),
            $Modal_Button = $('.JS-Modal-Button'),
            Duration      = 700;

        $Modal_Button.click(function () {
            $('#' + $(this).attr('data-modal-id')).fadeIn(Duration);
            return false;
        });

        $Modal_Close.click(function () {
            $(this).parents(Modal).fadeOut(Duration);
        });

        $(Modal).click(function () {
            $(this).fadeOut(Duration);
        });


        $Modal_Box.click(function () {
            return false;
        });
    });
})(jQuery);