/*
 * Copyright (c) 2015
 * Data Scripts - Mouse Parallax
 * Version 1.0.2
 * Create 2015.12.10
 * Author Bunker Labs

 * Usage:
 * Add class name 'JS-Mouse-Parallax'
 * Add class name 'JS-Mouse-Parallax-Layer'
 * data-parallax-rate
 * data-parallax-inversion - true | false
 * data-parallax-axis - both | x | y


 * Code structure:
 * <div class="JS-Mouse-Parallax">
 *     <div class="JS-Mouse-Parallax-Layer" data-parallax-rate='1'></div>
 *     <div class="JS-Mouse-Parallax-Layer" data-parallax-rate='0.8'></div>
 *     <div class="JS-Mouse-Parallax-Layer" data-parallax-rate='0.6'></div>
 * </div>
 */
(function ($) {
    $(document).ready(function () {
        var Layers = '.JS-Mouse-Parallax-Layer',
            Width = $(window).width() / 2,
            Height = $(window).height() / 2;

        $('.JS-Mouse-Parallax').each(function () {
            var Current_Parallax = $(this),
                Width = Current_Parallax.outerWidth() / 2,
                Height = Current_Parallax.outerHeight() / 2;


            $(Layers, Current_Parallax).each(function () {
                var Layer = $(this);
                var Axis = Layer.attr('data-parallax-axis') ? Layer.attr('data-parallax-axis') : 'any';

                $('.Header-Wrap').on("mousemove", function (event) {
                    var Position_X = event.pageX - Width;
                    var Position_Y = event.pageY - Height;
                    var Rate = $(Layer).attr('data-parallax-rate') ? Layer.attr('data-parallax-rate') : 1;

                    if (Axis == 'any') {
                        $(Layer).css({
                            'background-position': -Position_X * Rate + 'px ' + -Position_Y * Rate + 'px'
                        });
                    } else if (Axis == 'x') {
                        $(Layer).css({
                            'background-position': -Position_X * Rate - 30 + 'px' + '0px'
                        });
                    }

                });
            });
        });
    });
})(jQuery);