/*
 * Copyright (c) 2015
 * Data Scripts - Window
 * Version 1.0.0
 * Created 2015.12.22
 * Author Bunker Labs

 * Usage:
 * Add class name 'JS-Window-Button' - to activate button

 * Code structure:
 * <button class='JS-Window-Button' data-window-iframe='http://path.to/iframe'></button>
 */

(function ($) {
    $(document).ready(function () {
        var Window = '.JS-Window',
            Window_Box = '.JS-Window-Box',
            Window_Close = '.JS-Window-Close',
            $Window_Button = $('.JS-Window-Button'),
            Duration = 700;

        $Window_Button.click(function () {
            $('body').append(
                '<div class="JS-Window">' +
                '   <div class="JS-Window-Box">' +
                '       <div class="JS-Window-Close">×</div>' +
                '       <iframe src="' + $(this).attr('data-window-iframe') + '"></iframe>' +
                '   </div>' +
                '</div>'
            );

            $(Window).fadeIn(Duration);

            return false;
        });

        $(document).on('click', Window_Close, function () {
            $(this).parents(Window).fadeOut(Duration).promise().done(function () {
                $('.JS-Window').remove();
            });
        });

        $(document).on('click', Window, function () {
            $(this).fadeOut(Duration).promise().done(function () {
                $('.JS-Window').remove();
            });
        });

        $(document).on('click', Window_Box, function () {
            return false;
        });
    });
})(jQuery);