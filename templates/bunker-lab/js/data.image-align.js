/*
 * Copyright (c) 2015
 * Data Scripts - Image Align
 * Version 1.1.0
 * Create 2015.12.11
 * Author Bunker Labs

 * Usage:
 * Add class name 'JS-Image-Align' - to center inner image
 * Add attribute data-image-ratio      - to set image proportions: 16/9 | 3/4 | 1/1
 * Add attribute data-image-position   - to set image position: center | center/bottom | left/top | right/top

 * Code structure:
 * <div class="JS-Image-Align" data-image-ratio='16/9' data-image-position='center/top'><img src="..."></div>
 */
(function ($) {
    $(document).ready(function () {
        var $Images = $('.JS-Image-Align');
        $Images.find('img').css({
            'width'   : '100%',
            'position': 'relative'
        });

        $Images.each(function () {
            var Layout     = $(this),
                Img        = Layout.find('img'),
                Ratio      = Layout.attr('data-image-ratio') ? $(this).attr('data-image-ratio') : false,
                Position   = Layout.attr('data-image-position') ? $(this).attr('data-image-position') : 'center',
                Height     = Layout.outerWidth() / eval(Ratio),
                Position_X = 'center',
                Position_Y = 'center';

            if (Position != 'center') {
                Position   = Position.split('/');
                Position_X = Position[0];
                Position_Y = Position[1];
            }

            Layout.css({
                'height'               : Height,
                'background-image'     : 'url(' + Img.attr('src') + ')',
                'background-size'      : 'cover',
                'background-position-x': Position_X,
                'background-position-y': Position_Y
            });

            Img.remove();

            $(window).resize(function () {
                Layout.css({
                    'height': Layout.outerWidth() / eval(Ratio)
                });
            });
        });

    });
})(jQuery);