/*
 * Copyright (c) 2015
 * Data Scripts - Menu
 * Version 1.0.0
 * Create 2015.12.18
 * Author Bunker Labs

 * Usage:
 * 1)add structure
 * 2)add necessarily style
 * 3)set number value attribute 'data-height-menu'


 * Code structure:
 *  <div class="Script-Menu">
 *       <div class="Menu-Button" data-height-menu="6">
 *          <span>Click Here</span>
 *       </div>
 *       <div class="Menu-List">
 *           <ul>
 *               <li>Item 1</li>
 *               <li>Item 2</li>
 *               <li>Item 3</li>
 *               <li>Item 4</li>
 *               <li>Item 5</li>
 *               <li>Item 6</li>
 *               <li>Item 7</li>
 *               <li>Item 8</li>
 *               <li>Item 9</li>
 *               <li>Item 10</li>
 *           </ul>
 *       </div>
 *   </div>


* Necessarily css
*
*   .Menu-List{
*        display: none;
*        height: 0;
*        overflow-y: scroll;
*        transition: height 0.2s linear;
*    }
*

 */
(function ($) {
    $(document).ready(function () {


        $('.Script-Menu .Menu-Button').click(function(){

            var $Button = $(this);

            if($Button.parents('.Script-Menu').find('.Menu-List').css('display','none')){

                   var Actual_Height = $Button.parents('.Script-Menu').find('.Menu-List li').actual('outerHeight'),
                       Number_Size_Menu = $Button.attr('data-height-menu'),
                       All_Height_List = Actual_Height * Number_Size_Menu;

                $('.Script-Menu .Menu-List').css({'display':'none', 'height':'0px'});

                $Button.parents('.Script-Menu').find('.Menu-List').css({'display':'block','height':All_Height_List});

            }else {
                $Button.parents('.Script-Menu').find('.Menu-List').css({'display':'none','height':'0px'});
            }

            return false;
        });

        $('.Script-Menu .Menu-List li').click(function(){

            $(this).parents('.Script-Menu .Menu-List').find('li').removeClass('Item-List-Active');

            $(this).addClass('Item-List-Active');

        });



        $('body, html').click(function(){

            $('.Script-Menu .Menu-List').css({'display': 'none', 'height':'0px'});

            return false;
        });

    });


})(jQuery);