@charset "UTF-8";

///
/// Creates a list of global breakpoints
///
/// @example scss - Creates a single breakpoint with the label `phone`
///  $Viewports: ('phone': 320px);
///



///
/// Creates a list of static expressions or media types
///
/// @example scss - Creates a single media type (screen)
///  $$Viewport-Expressions: ('screen': 'screen');
///
/// @example scss - Creates a static expression with logical disjunction (OR operator)
///  $$Viewport-Expressions: (
///    'retina2x': '(-webkit-min-device-pixel-ratio: 2), (min-resolution: 192dpi)'
///  );
///


///
/// Defines a number to be added or subtracted from each unit when declaring breakpoints with exclusive intervals
///
/// @example scss - Interval for pixels is defined as `1` by default
///  @include media('>128px') {}
///
///  /* Generates: */
///  @media (min-width: 129px) {}
///
/// @example scss - Interval for ems is defined as `0.01` by default
///  @include media('>20em') {}
///
///  /* Generates: */
///  @media (min-width: 20.01em) {}
///
/// @example scss - Interval for rems is defined as `0.1` by default, to be used with `font-size: 62.5%;`
///  @include media('>2.0rem') {}
///
///  /* Generates: */
///  @media (min-width: 2.1rem) {}
///
$unit-intervals: (
        'px': 1,
        'em': 0.01,
        'rem': 0.1
) !default;

///
/// Defines whether support for media queries is available, useful for creating separate stylesheets
/// for browsers that don't support media queries.
///
/// @example scss - Disables support for media queries
///  $im-media-support: false;
///  @include media('>=tablet') {
///    .foo {
///      color: tomato;
///    }
///  }
///
///  /* Generates: */
///  .foo {
///    color: tomato;
///  }
///
$im-media-support: true !default;

///
/// Selects which breakpoint to emulate when support for media queries is disabled. Media queries that start at or
/// intercept the breakpoint will be displayed, any others will be ignored.
///
/// @example scss - This media query will show because it intercepts the static breakpoint
///  $im-media-support: false;
///  $im-no-media-breakpoint: 'desktop';
///  @include media('>=tablet') {
///    .foo {
///      color: tomato;
///    }
///  }
///
///  /* Generates: */
///  .foo {
///    color: tomato;
///  }
///
/// @example scss - This media query will NOT show because it does not intercept the desktop breakpoint
///  $im-media-support: false;
///  $im-no-media-breakpoint: 'tablet';
///  @include media('>=desktop') {
///    .foo {
///      color: tomato;
///    }
///  }
///
///  /* No output */
///
$im-no-media-breakpoint: 'desktop' !default;

///
/// Selects which media expressions are allowed in an expression for it to be used when media queries
/// are not supported.
///
/// @example scss - This media query will show because it intercepts the static breakpoint and contains only accepted media expressions
///  $im-media-support: false;
///  $im-no-media-breakpoint: 'desktop';
///  $im-no-$Viewport-Expressions: ('screen');
///  @include media('>=tablet', 'screen') {
///    .foo {
///      color: tomato;
///    }
///  }
///
///   /* Generates: */
///   .foo {
///     color: tomato;
///   }
///
/// @example scss - This media query will NOT show because it intercepts the static breakpoint but contains a media expression that is not accepted
///  $im-media-support: false;
///  $im-no-media-breakpoint: 'desktop';
///  $im-no-$Viewport-Expressions: ('screen');
///  @include media('>=tablet', 'retina2x') {
///    .foo {
///      color: tomato;
///    }
///  }
///
///  /* No output */
///
$im-no-Viewport-Expressions: ('screen', 'portrait', 'landscape') !default;

////
/// Cross-engine logging engine
/// @author Hugo Giraudel
/// @access private
////






///
/// Add `$unit` to `$value`
///
/// @param {Number} $value - Value to add unit to
/// @param {String} $unit - String representation of the unit
///
/// @return {Number} - `$value` expressed in `$unit`
///
@function to-length($value, $unit) {
    $units: ('px': 1px, 'cm': 1cm, 'mm': 1mm, '%': 1%, 'ch': 1ch, 'pc': 1pc, 'in': 1in, 'em': 1em, 'rem': 1rem, 'pt': 1pt, 'ex': 1ex, 'vw': 1vw, 'vh': 1vh, 'vmin': 1vmin, 'vmax': 1vmax);

    @if not index(map-keys($units), $unit) {
        $_: log('Invalid unit `#{$unit}`.');
    }

    @return $value * map-get($units, $unit);
}

///
/// This mixin aims at redefining the configuration just for the scope of
/// the call. It is helpful when having a component needing an extended
/// configuration such as custom breakpoints (referred to as tweakpoints)
/// for instance.
///
/// @author Hugo Giraudel
///
/// @param {Map} $tweakpoints [()] - Map of tweakpoints to be merged with `$Viewports`
/// @param {Map} $tweak-$Viewport-Expressions [()] - Map of tweaked media expressions to be merged with `$media-expression`
///
/// @example scss - Extend the global breakpoints with a tweakpoint
///  @include media-context(('custom': 678px)) {
///    .foo {
///      @include media('>phone', '<=custom') {
///       // ...
///      }
///    }
///  }
///
/// @example scss - Extend the global media expressions with a custom one
///  @include media-context($tweak-$Viewport-Expressions: ('all': 'all')) {
///    .foo {
///      @include media('all', '>phone') {
///       // ...
///      }
///    }
///  }
///
/// @example scss - Extend both configuration maps
///  @include media-context(('custom': 678px), ('all': 'all')) {
///    .foo {
///      @include media('all', '>phone', '<=custom') {
///       // ...
///      }
///    }
///  }
///
@mixin media-context($tweakpoints: (), $tweak-Viewport-Expressions: ()) {
    // Save global configuration
    $global-breakpoints: $Viewports;
    $global-Viewport-Expressions: $Viewport-Expressions;

    // Update global configuration
    $Viewports: map-merge($Viewports, $tweakpoints) !global;
    $Viewport-Expressions: map-merge($Viewport-Expressions, $tweak-Viewport-Expressions) !global;

    @content;

    // Restore global configuration
    $Viewports: $global-breakpoints !global;
    $Viewport-Expressions: $global-Viewport-Expressions !global;
}

////
/// include-media public exposed API
/// @author Eduardo Boucas
/// @access public
////


///
/// Generates a media query based on a list of conditions
///
/// @param {Arglist}   $conditions  - Media query conditions
///
/// @example scss - With a single set breakpoint
///  @include media('>phone') { }
///
/// @example scss - With two set breakpoints
///  @include media('>phone', '<=tablet') { }
///
/// @example scss - With custom values
///  @include media('>=358px', '<850px') { }
///
/// @example scss - With set breakpoints with custom values
///  @include media('>desktop', '<=1350px') { }
///
/// @example scss - With a static expression
///  @include media('retina2x') { }
///
/// @example scss - Mixing everything
///  @include media('>=350px', '<tablet', 'retina3x') { }
///
@mixin Adaptive($conditions...) {
    // scss-lint:disable SpaceAroundOperator
    @if ($im-media-support and length($conditions) == 0) or
      (not $im-media-support and im-intercepts-static-breakpoint($conditions...)) {
        @content;
    } @else if ($im-media-support and length($conditions) > 0) {
        @media #{unquote(parse-expression(nth($conditions, 1)))} {
            // Recursive call
            @include Adaptive( slice( $conditions , 2 )... ) {
            @content;
        }
        }
    }
}