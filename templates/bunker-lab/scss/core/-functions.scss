// ---------------------------------------------------------------------------------------------------------------------
// Function with no `@return` called next to `@warn` in Sass 3.3
// to trigger a compiling error and stop the process.
// ---------------------------------------------------------------------------------------------------------------------
@function noop() {
}
// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
// Log a message either with `@error` if supported
// else with `@warn`, using `feature-exists('at-error')`
// to detect support.
//
// @param {String} $message - Message to log
// ---------------------------------------------------------------------------------------------------------------------
@function log($message) {
    @if feature-exists('at-error') {
        @error $message;
    }
    @else
    {
        @warn $message;
        $_ : noop();
    }

    @return $message;
}

// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
// Casts a string into a number
//
// @param {String | Number} $value - Value to be parsed
//
// @return {Number}
// ---------------------------------------------------------------------------------------------------------------------
@function To-Number($value) {
    @if type-of($value) == 'number'
    {
        @return $value;
    } @else if type-of($value) != 'string'
    {
        $_ : log('Value for `to-number` should be a number or a string.');
    }

    $result : 0;
    $digits : 0;
    $minus : str-slice($value, 1, 1) == '-';
    $numbers : ('0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9);

    @for $i from if($minus, 2, 1) through str-length($value)
    {
        $character : str-slice($value, $i, $i);

        @if not (index(map-keys($numbers), $character) or $character == '.')
        {
            @return to-length(if($minus, -$result, $result), str-slice($value, $i))
        }

        @if $character == '.'
        {
            $digits : 1;
        }
        @else if $digits == 0
        {
            $result : $result * 10 + map-get($numbers, $character);
        }
        @else
        {
            $digits : $digits * 10;
            $result : $result + map-get($numbers, $character) / $digits;
        }
    }

    @return if($minus, -$result, $result);
}
// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
// Wrapper mixin for the log function so it can be used with a more friendly
// API than `@if log('..') {}` or `$_: log('..')`. Basically, use the function
// within functions because it is not possible to include a mixin in a function
// and use the mixin everywhere else because it's much more elegant.
//
// @param {String} $message - Message to log
// ---------------------------------------------------------------------------------------------------------------------
@mixin log($message)
{
    @if log($message)
    {}
}

// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
// Get operator of an expression
//
// @param {String} $expression - Expression to extract operator from
//
// @return {String} - Any of `>=`, `>`, `<=`, `<`, `≥`, `≤`
// ---------------------------------------------------------------------------------------------------------------------
@function get-expression-operator($expression)
{
    @each $operator in ('>=', '>', '<=', '<', '≥', '≤')
    {
        @if str-index($expression, $operator)
        {
            @return $operator;
        }
    }

    // It is not possible to include a mixin inside a function, so we have to
    // rely on the `log(..)` function rather than the `log(..)` mixin. Because
    // functions cannot be called anywhere in Sass, we need to hack the call in
    // a dummy variable, such as `$_`. If anybody ever raise a scoping issue with
    // Sass 3.3, change this line in `@if log(..) {}` instead.
    $_ : log('No operator found in `#{$expression}`.');
}
// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
// Get dimension of an expression, based on a found operator
//
// @param {String} $expression - Expression to extract dimension from
// @param {String} $operator - Operator from `$expression`
//
// @return {String} - `width` or `height` (or potentially anything else)
// ---------------------------------------------------------------------------------------------------------------------
@function get-expression-dimension($expression, $operator)
{
    $operator-index : str-index($expression, $operator);
    $parsed-dimension : str-slice($expression, 0, $operator-index - 1);
    $dimension : 'width';

    @if str-length($parsed-dimension) > 0
    {
        $dimension : $parsed-dimension;
    }

    @return $dimension;
}
// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
// Get dimension prefix based on an operator
//
// @param {String} $operator - Operator
//
// @return {String} - `min` or `max`
// ---------------------------------------------------------------------------------------------------------------------
@function get-expression-prefix($operator)
{
    @return if(index(('<', '<=', '≤'), $operator), 'max', 'min');
}
// ---------------------------------------------------------------------------------------------------------------------

///
/// Get value of an expression, based on a found operator
///
/// @param {String} $expression - Expression to extract value from
/// @param {String} $operator - Operator from `$expression`
///
/// @return {Number} - A numeric value
///
@function get-expression-value($expression, $operator)
{
    $operator-index : str-index($expression, $operator);
    $value : str-slice($expression, $operator-index + str-length($operator));

    @if map-has-key($Viewports, $value) {
        $value : map-get($Viewports, $value);
    } @else
    {
        $value : To-Number($value);
    }

    $interval : map-get($unit-intervals, unit($value));

    @if not $interval
    {
        // It is not possible to include a mixin inside a function, so we have to
        // rely on the `log(..)` function rather than the `log(..)` mixin. Because
        // functions cannot be called anywhere in Sass, we need to hack the call in
        // a dummy variable, such as `$_`. If anybody ever raise a scoping issue with
        // Sass 3.3, change this line in `@if log(..) {}` instead.
        $_ : log('Unknown unit `#{unit($value)}`.');
    }

    @if $operator == '>'
    {
        $value : $value + $interval;
    }
    @else if $operator == '<'
    {
        $value : $value - $interval;
    }

    @return $value;
}

// ---------------------------------------------------------------------------------------------------------------------
// Parse an expression to return a valid media-query expression
//
// @param {String} $expression - Expression to parse
//
// @return {String} - Valid media query
// ---------------------------------------------------------------------------------------------------------------------
@function parse-expression($expression) {
    // If it is part of $Viewport-Expressions, it has no operator
    // then there is no need to go any further, just return the value
    @if map-has-key($Viewport-Expressions, $expression) {
        @return map-get($Viewport-Expressions, $expression);
    }

    $operator : get-expression-operator($expression);
    $dimension : get-expression-dimension($expression, $operator);
    $prefix : get-expression-prefix($operator);
    $value : get-expression-value($expression, $operator);

    @return '(#{$prefix}-#{$dimension}: #{$value})';
}
// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
/// Slice `$list` between `$start` and `$end` indexes
///
/// @access private
///
/// @param {List} $list - List to slice
/// @param {Number} $start [1] - Start index
/// @param {Number} $end [length($list)] - End index
///
/// @return {List} Sliced list
// ---------------------------------------------------------------------------------------------------------------------
@function slice($list, $start: 1, $end: length($list)) {
    @if length($list) < 1 or $start > $end
    {
        @return ();
    }

    $result : ();

    @for $i from $start through $end
    {
        $result : append($result, nth($list, $i));
    }

    @return $result;
}
// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
// String to number converter
// @author Hugo Giraudel
// @access private
// ---------------------------------------------------------------------------------------------------------------------


// ---------------------------------------------------------------------------------------------------------------------
// Determines whether a list of conditions is intercepted by the static breakpoint.
//
// @param {Arglist}   $conditions  - Media query conditions
//
// @return {Boolean} - Returns true if the conditions are intercepted by the static breakpoint
// ---------------------------------------------------------------------------------------------------------------------
@function im-intercepts-static-breakpoint($conditions...)
{
    $no-media-breakpoint-value : map-get($Viewports, $im-no-media-breakpoint);

    @if not $no-media-breakpoint-value
    {
        @if log('`#{$im-no-media-breakpoint}` is not a valid breakpoint.')
        {}
    }

    @each $condition in $conditions
    {
        @if not map-has-key($Viewport-Expressions, $condition)
        {
            $operator : get-expression-operator($condition);
            $prefix : get-expression-prefix($operator);
            $value : get-expression-value($condition, $operator);

            // scss-lint:disable SpaceAroundOperator
            @if ($prefix == 'max' and $value <= $no-media-breakpoint-value) or
          ($prefix == 'min' and $value > $no-media-breakpoint-value)
            {
                @return false;
            }
        }
        @else if not index($im-no-Viewport-Expressions, $condition)
        {
            @return false;
        }
    }

    @return true;
}
// ---------------------------------------------------------------------------------------------------------------------