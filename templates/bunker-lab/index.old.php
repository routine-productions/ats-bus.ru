﻿`<?php defined('_JEXEC') or die('Restricted access'); ?>

<!DOCTYPE html>
<html>
<head>
    <title>ATS-BUS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <link rel="stylesheet" href="/templates/bunker-lab/css/init.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,700italic' rel='stylesheet' type='text/css'>

    <script src="/templates/bunker-lab/js/dependencies/jquery.min.js"></script>
    <script src="/templates/bunker-lab/js/dependencies/jquery.actual.min.js"></script>
    <script src="/templates/bunker-lab/js/dependencies/wow.min.js"></script>

    <script src="/templates/bunker-lab/js/jquery.pagenav.js"></script>
    <script src="/templates/bunker-lab/js/index.js"></script>
    <script src="/templates/bunker-lab/js/data.modal.js"></script>
    <script src="/templates/bunker-lab/js/data.vertical-align.js"></script>
    <script src="/templates/bunker-lab/js/data.mobile-menu.js"></script>
    <script src="/templates/bunker-lab/js/data.form.js"></script>


    <script>new WOW().init();</script>
</head>
<body class="wow fadeIn">

<header class="Site-Header">

    <div class="Top">
        <img src="/img/logo.svg" alt="ATS-BUS Logo" class="Company-Logo">
        <ul class="Phones-List">
            <li class="Phone-Item">
                <small class="Phone-Operator">МТС Украина</small>
                <p class="Phone-Number tel">xx</p>
            </li>
            <li class="Phone-Item">
                <small class="Phone-Operator">МТС Россия</small>
                <p class="Phone-Number tel">xx</p>
            </li>
            <li class="Phone-Item">
                <small class="Phone-Operator">Феникс ДНР</small>
                <p class="Phone-Number tel">xx</p>
            </li>
        </ul>
    </div>

    <div class="Middle">

        <nav role="navigation">
            <ul class="Navigation-List">
                <li class="Navigation-Item">
                    <a href="#" class="Navigation-Link">xx</a>
                    <a href="#" class="Navigation-Link">xx</a>
                    <a href="#" class="Navigation-Link">xx</a>
                    <a href="#" class="Navigation-Link">xx</a>
                    <a href="#" class="Navigation-Link">xx</a>
                </li>
            </ul>
        </nav>

        <form action="" class="Order-Callback">
            <h2>Заказ билетов на автобус из Донецка в Москву, Тулу, Воронеж и обратно</h2>
            <input type="text">
            <button class="Button">Перезвоните мне</button>
        </form>

    </div>

</header>

<main>
    <div class="Routes">
        <ul class="Routes-List">
            <li class="Routes-Item Tab-Caption">
                <img src="" class="City-Photo" alt="">
                <a class="Routes-Link" href="">xx</a>
            </li>

            <li class="Routes-Item Tab-Caption">
                <img src="" class="City-Photo" alt="">
                <a class="Routes-Link" href="">xx</a>
            </li>

            <li class="Routes-Item Tab-Caption">
                <img src="" class="City-Photo" alt="">
                <a class="Routes-Link" href="">xx</a>
            </li>

            <li class="Routes-Item Tab-Caption">
                <img src="" class="City-Photo" alt="">
                <a class="Routes-Link" href="">xx</a>
            </li>

            <li class="Routes-Item Tab-Caption">
                <img src="" class="City-Photo" alt="">
                <a class="Routes-Link" href="">xx</a>
            </li>
        </ul>

        <ul class="Tabs-Content">
            <li class=""></li>
        </ul>

    </div>
</main>

<div class="Fixed-Navigation">
    <nav class="Node Script-Menu">
        <a href="/" class="Logotype"></a>
        <a class="Hamburger Script-Menu-Toggle"><span></span></a>
        <ul class="Menu-List Script-Menu-List">
            <li><a class="Page-Nav" href="#Routes">Маршруты</a></li>
            <li><a class="Page-Nav" href="#Advantages">Преимущества</a></li>
            <li><a class="Page-Nav" href="#Discounts">Скидки на билеты</a></li>
            <li><a class="Page-Nav" href="#About-Us">О компании</a></li>
            <li><a class="Page-Nav" href="#Site-Footer">Контакты</a></li>
        </ul>
    </nav>
</div>

<header id="Site-Header">
    <div id="Top">
        <nav class="Node Script-Menu">
            <a href="/" class="Logotype"></a>
            <a class="Hamburger Script-Menu-Toggle"><span></span></a>
            <ul class="Menu-List Script-Menu-List">
                <li><a class="Page-Nav" href="#Routes">Маршруты</a></li>
                <li><a class="Page-Nav" href="#Advantages">Преимущества</a></li>
                <li><a class="Page-Nav" href="#Discounts">Скидки на билеты</a></li>
                <li><a class="Page-Nav" href="#About-Us">О компании</a></li>
                <li><a class="Page-Nav" href="#Site-Footer">Контакты</a></li>
            </ul>
        </nav>
    </div>
    <div id="Middle" class="Node">
        <div id="Header-Title" class="wow fadeInLeft" data-wow-duration="1s">
            <img class="Header-Img" src="/templates/bunker-lab/img/pic-route.png" alt="">

            <h1>Автобусные рейсы
                <small>из Донецка в Москву, Тулу и обратно</small>
            </h1>
        </div>
        <aside id="Offer" class="wow fadeInRight" data-wow-delay="1s" data-wow-duration="1.3s">
            <div class="Offer-Content">
                <h4 class="Offer-Title">Закажите билеты по&nbsp;телефону
                    <small>и&nbsp;получите скидку</small>
                </h4>
                <ul class="Phone-Numbers vcard">
                    <li>
                        <span class="Operator">МТС Украина</span>
                        <span class="tel"><jdoc:include type="modules" name="mtc_ua"/></span>
                    </li>
                    <li>
                        <span class="Operator">МТС Россия</span>
                        <span class="tel"><jdoc:include type="modules" name="mtc_ru"/></span>
                    </li>
                    <li>
                        <span class="Operator">Феникс ДНР</span>
                        <span class="tel"><jdoc:include type="modules" name="fenix_dnr"/></span>
                    </li>
                </ul>
                <a class="Callback-Button Small Script-Modal-Button" href="#" data-modal-id="Modal-Form">
                    <img src="/templates/bunker-lab/img/ico-phone.png" alt="">Заказать звонок
                </a>
            </div>

            <form action="/" method="POST" class="Offer-Form">
                <h4 class="Offer-Title">Введите свой номер телефона и мы вам перезвоним
                </h4>
                <input type="text" name="phone" placeholder="Ваш номер телефона">
                <a class="Callback-Button Ajax"><img src="/templates/bunker-lab/img/ico-phone.png" alt="">Перезвоните
                    мне</a>

                <div class="Message"></div>
            </form>
        </aside>
    </div>
</header>

<main>

    <jdoc:include type="component"/>

    <section id="Advantages" class="Node wow fadeIn" data-wow-offset="200">
        <header>
            <h3 class="Section-Heading">Наши преимущества</h3>

            <p>Почему наши клиенты выбирают компанию <nobr>ATS-BUS</nobr></p>
        </header>
        <ul class="Advantages-List">
            <li class="Advantage-Item wow fadeIn" data-wow-offset="100">
                <img src="/templates/bunker-lab/img/ico-advantage-01.png" alt="" class="Advantage-Icon">
                <div>
                    <h4>Чистый и исправный автобус</h4>
                    <p>Поездка пройдет безопасно и с комфортом</p>
                </div>
            </li>
            <li class="Advantage-Item wow fadeIn" data-wow-offset="100">
                <img src="/templates/bunker-lab/img/ico-advantage-02.png" alt="" class="Advantage-Icon">
                <div>
                    <h4>Быстрое прохождение таможни</h4>
                    <p>Отлаженная система без простоев</p>
                </div>
            </li>
            <li class="Advantage-Item wow fadeIn" data-wow-offset="100" data-wow-delay=".4s">
                <img src="/templates/bunker-lab/img/ico-advantage-03.png" alt="" class="Advantage-Icon">
                <div>
                    <h4>Опытный и ответственный водитель</h4>
                    <p>Мы набираем только надежных профессионалов не менее чем с 10 годами безаварийного стажа</p>
                </div>
            </li>
            <li class="Advantage-Item wow fadeIn" data-wow-offset="100" data-wow-delay=".4s">
                <img src="/templates/bunker-lab/img/ico-advantage-04.png" alt="" class="Advantage-Icon">
                <div>
                    <h4>Система скидок постоянным клиентам</h4>
                    <p>При заказе билетов по телефону</p>
                </div>
            </li>
            <li class="Advantage-Item wow fadeIn" data-wow-offset="100" data-wow-delay=".8s">
                <img src="/templates/bunker-lab/img/ico-advantage-05.png" alt="" class="Advantage-Icon">
                <div>
                    <h4>Отправка посылок</h4>
                    <p>Безопасная и дешевая доставка небольших отправлений</p>
                </div>
            </li>
            <li class="Advantage-Item wow fadeIn" data-wow-offset="100" data-wow-delay=".8s">
                <img src="/templates/bunker-lab/img/ico-advantage-06.png" alt="" class="Advantage-Icon">
                <div>
                    <h4>Внимательные и заботливые стюарды</h4>
                    <p>Мы всегда заботимся о вашем комфорте</p>
                </div>
            </li>
        </ul>
    </section>
    <section id="Discounts" class="Node">
        <h3 class="Section-Heading">Наши скидки</h3>

        <ul class="Discounts-List">

            <li class="Discount-Item wow fadeIn" data-wow-offset="100">
                <div class="Discount-Icon">
                    <span>50%</span>
                </div>
                <p>Детский билет</p>
            </li>
            <li class="Discount-Item wow fadeIn" data-wow-offset="100" data-wow-delay=".2s">
                <div class="Discount-Icon">
                    <span>10%</span>
                </div>
                <p>Каждая третья поездка</p>
            </li>
            <li class="Discount-Item wow fadeIn" data-wow-offset="100" data-wow-delay=".4s">
                <div class="Discount-Icon">
                    <span>20%</span>
                </div>

                <p>Каждая пятая поездка</p>
            </li>
            <li class="Discount-Item wow fadeIn" data-wow-offset="100" data-wow-delay=".6s">
                <div class="Discount-Icon">
                    <span class="Small">бесплатно</span>
                </div>
                <p>Каждая десятая поездка</p>
            </li>
        </ul>
        <div class="Warning-Message">
            Скидки предоставляются только при заказе билетов по телефону
        </div>
    </section>
    <section id="About-Us" class="Node">

        <article>
            <img class="About-Pic" src="/templates/bunker-lab/img/pic-about.png" alt="">
            <h3 class="About-Heading">О нас</h3>
            <jdoc:include type="modules" name="about"/>
        </article>
    </section>
</main>
<footer id="Site-Footer">
    <div class="Node">
        <div class="Copyright">
            <p> &copy; 2015 &mdash; <strong>ATS-BUS</strong></p>

            <p>Пассажирские перевозки из Донецка в Москву и Тулу</p>
        </div>
        <ul class="Phone-Numbers">
            <li>
                <span class="Operator">МТС Украина</span>
                <span class="tel"><jdoc:include type="modules" name="mtc_ua"/></span>
            </li>
            <li>
                <span class="Operator">МТС Россия</span>
                <span class="tel"><jdoc:include type="modules" name="mtc_ru"/></span>
            </li>
            <li>
                <span class="Operator">Феникс ДНР</span>
                <span class="tel"><jdoc:include type="modules" name="fenix_dnr"/></span>
            </li>
            <li>
                <span class="Operator">e&ndash;mail</span>
                <span class="email"><jdoc:include type="modules" name="email"/></span>
            </li>
        </ul>
    </div>
</footer>

<div class='Script-Modal' id='Modal-Form'>
    <div class='Script-Modal-Box Script-Vertical-Align'>
        <form class='Script-Form'
              data-form-email='olegblud@gmail.com,ats-bus@yandex.ru'
              data-form-subject='ATS-BUS: Перезвоните мне'
              data-form-url='/templates/bunker-lab/js/data.form.php'>
            <h4 class="Offer-Title">Введите свой номер телефона и мы вам перезвоним</h4>
            <input type='text' name='phone' data-input-title='Телефон' placeholder="Телефон">
            <button class='Script-Form-Button Callback-Button'><img src="/templates/bunker-lab/img/ico-phone.png"
                                                                    alt="">Перезвоните мне
            </button>
            <div class="Script-Form-Result"></div>
        </form>

        <div class='Script-Modal-Close'>×</div>
    </div>
</div>

</body>
</html>
