<?php
defined('_JEXEC') or die;
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);

$Extra_Fields = [];
if (isset($this->item->extra_fields)) {
    foreach ($this->item->extra_fields as $Key => $Value) {
        $Extra_Fields[$Value->alias] = $Value->value;
    }
}
?>

<li data-tab="<?= $this->item->alias ?>">
    <header>
        <?php if (isset($Extra_Fields['to_route'])) { ?>
            <h3>Рейс из <?= $Extra_Fields['to_route'] ?></h3>
        <?php } ?>
        <small>Маршрут <?= $this->item->title ?></small>
    </header>
    <?php if (isset($Extra_Fields['departure_time']) && isset($Extra_Fields['days_of_departure'])) { ?>
        <dl>
            <dt>время отправления:</dt>
            <dd><span
                    class="Time"><?= $Extra_Fields['departure_time'] ?></span> <?= $Extra_Fields['days_of_departure'] ?>
            </dd>
        </dl>
    <?php } ?>

    <?php if (isset($Extra_Fields['departure_point'])) { ?>
        <dl>
            <dt>место отправления:</dt>
            <dd><?= $Extra_Fields['departure_point'] ?></dd>
        </dl>
    <?php } ?>


    <?php if (isset($Extra_Fields['arrival_time']) && isset($Extra_Fields['days_of_arrival'])) { ?>

        <dl>
            <dt>время прибытия:</dt>
            <dd><span class="Time"><?= $Extra_Fields['arrival_time'] ?></span> <?= $Extra_Fields['days_of_arrival'] ?>
            </dd>
        </dl>
    <?php } ?>



    <?php if (isset($Extra_Fields['point_of_arrival'])) { ?>

        <dl>
            <dt>место прибытия:</dt>
            <dd><?= $Extra_Fields['point_of_arrival'] ?></dd>
        </dl>
    <?php } ?>

    <?php if (isset($Extra_Fields['ticket_price'])) { ?>
        <dl>
            <dt>стоимость:</dt>
            <dd><span class="Price"><?= $Extra_Fields['ticket_price'] ?> руб.</span></dd>
        </dl>
    <?php } ?>


    <?php if (isset($Extra_Fields['map_link']) && isset($Extra_Fields['map'])) { ?>
        <footer>
            <a class="Show-on-Map JS-Window-Button"
               href="<?= $Extra_Fields['map_link'] ?>"
               data-window-iframe="<?= $Extra_Fields['map'] ?>"
               target="_blank">Посмотреть маршрут на карте</a>
        </footer>
    <?php } ?>
</li>





